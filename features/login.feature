Feature: Log into the system

	Scenario: Valid user
		Given The user has registered in the system
		When The user inserts a valid identification
		Then The user will be granted access to the system 
	
	Scenario: Invalid user
		Given the user has registered in the system
		When the user inserts a wrong identification
		Then The user will not be granted access to the system
		