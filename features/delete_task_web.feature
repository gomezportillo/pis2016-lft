Feature: Delete a task through the web

	Scenario: Deleted correctly
		Given The user has at least one task
		When The user selects it and clicks on Delete button 
		Then The task will be deleted