Feature: Log into the system from the web

	Scenario: Valid user
		Given The user goes to the webpage
		When The user inserts a valid identification in the web
		Then The user will be granted access to the system and access to the task view
		And The browser gets closed
		
	Scenario: Invalid user
		Given The user goes to the webpage
		When The user inserts a invalid identification in the web
		Then The user will not be granted access to the system and access to the task view
		And The browser gets closed

		