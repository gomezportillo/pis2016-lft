package test;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitCreateUser {

	Usuario user;
	
	@Given("^The admin creates a user$")
	public void The_admin_creates_a_user() throws Throwable {
		user = new Usuario ("manoli", "1234", "standard");
	}

	@When("^The admin insert all data$")
	public void The_admin_insert_all_data() throws Throwable {
	    user.create();
	}

	@Then("^The user is in daba base$")
	public void The_user_is_in_daba_base() throws Throwable {
		assertTrue(user.read());
			    
			    tearDown();
	}
	
	public void tearDown() {
		user.delete();
	}

}
