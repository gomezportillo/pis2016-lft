package test;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;

public class JUnitCrearTarea {
	
	Tarea tasktest;
	
	@Given("^The user creates a task$")
	public void The_user_creates_a_task() throws Throwable {
	    tasktest = new Tarea ("Mandar email", "manolo", "alta", "12/12/2013", "proyecto1", "notas", "sin empezar");
	}

	@When("^The user inserts all data$")
	public void The_user_inserts_all_datas() throws Throwable {
		tasktest.create();
	}

	@Then("^The task is in daba base$")
	public void The_task_is_in_daba_base() throws Throwable {
	    assertTrue(tasktest.read());
	    
	    tearDown();
	}
	
	public void tearDown() {
		tasktest.delete();
	}

}
