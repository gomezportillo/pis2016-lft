package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;
public class JUnitregistrar {

	Usuario usertest;

	@Given("^The user is not registered in the database$")
	public void The_user_is_not_registered_in_the_database() throws Throwable {
		usertest  = new Usuario ("pablo", "1234", "standard");
		assertFalse(usertest.read());
	}

	@When("^The user gives a name and a password$")
	public void The_user_inserts_a_valid_set_of_data() throws Throwable {
		usertest.create();
	}

	@Then("^The user will be registered$")
	public void The_user_will_be_registered() throws Throwable {
		assertTrue(usertest.read());
		
		tearDown();
	}

	public void tearDown() {
		usertest.delete();
	}

}
