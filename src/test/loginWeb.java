package test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginWeb {

	WebDriver driver = new ChromeDriver();

	@Given("^The user goes to the webpage$")
	public void The_user_goes_to_the_webpage() throws Throwable {
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		driver.get("localhost:8080/Sprint_1");
	}

	@When("^The user inserts a valid identification in the web$")
	public void The_user_inserts_a_valid_identification_in_the_web() throws Throwable {
		WebElement input_nombre = driver.findElement(By.name("nombre"));
		input_nombre.sendKeys("pedroma");
		
		WebElement input_pwd = driver.findElement(By.name("pass"));
		input_pwd.sendKeys("1234");
		
		WebElement btn_acceder = driver.findElement(By.xpath("//input[@value='Acceder']"));
		btn_acceder.click();
	}

	@Then("^The user will be granted access to the system and access to the task view$")
	public void The_user_will_be_granted_access_to_the_system_and_access_to_the_task_view() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("VISTA DE TAREAS DE"));
		
	}
	
	@Then("^The browser gets closed$")
	public void The_browser_gets_closed() throws Throwable {
	    driver.quit();
	}
	
	@When("^The user inserts a invalid identification in the web$")
	public void The_user_inserts_a_invalid_identification_in_the_web() throws Throwable {
		WebElement input_nombre = driver.findElement(By.name("nombre"));
		input_nombre.sendKeys("usuario que no existe");
		
		WebElement input_pwd = driver.findElement(By.name("pass"));
		input_pwd.sendKeys("contrase�a que est� mal");
		
		WebElement btn_acceder = driver.findElement(By.xpath("//input[@value='Acceder']"));
		btn_acceder.click();
	}

	@Then("^The user will not be granted access to the system and access to the task view$")
	public void The_user_will_not_be_granted_access_to_the_system_and_access_to_the_task_view() throws Throwable {
		WebElement cabecera = driver.findElement(By.id("cabecera"));
		assertTrue(cabecera.getText().contains("Usuario o contrase�a incorrectas"));
		driver.quit();
	}
}
