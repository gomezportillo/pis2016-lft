package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Tarea;

public class JUnitModifyTask {

	Tarea tasktest, tasktest2;

	@Given("^The user has a created task$")
	public void The_user_has_a_created_task() throws Throwable {
		tasktest = new Tarea ("Mandar", "manolo", "alta", "12/12/2013", "proyecto1", "notas", "sin empezar");
		tasktest.create();
		assertTrue(tasktest.read());
	}

	@When("^The user modifies some data of this task$")
	public void The_user_modifies_some_data_of_this_task() throws Throwable {
		tasktest2 = new Tarea ("Mandar email", "MIGUEL", "alta", "12/12/2013", "proyecto1", "notas", "sin empezar");

		assertFalse(tasktest.equals(tasktest2));
		tasktest.update(tasktest2);
	}

	@Then("^The task is correctly stored in the database$")
	public void The_task_is_correctly_stored_in_the_database() throws Throwable {
		assertFalse(tasktest.read());
		assertTrue(tasktest2.read());
		
		tearDown();
	}

	public void tearDown() {
		tasktest.delete();
		tasktest2.delete();
	}
}