package test;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitLogin {

	Usuario usertest, usertest2;
	
	@Given("^The user has registered in the system$")
	public void The_user_has_registered_in_the_system() throws Throwable {
		usertest = new Usuario ("manolo", "1234", "standard");
		usertest.create();
		assertTrue(usertest.read());
	}

	@When("^The user inserts a valid identification$")
	public void The_user_inserts_a_valid_identification() throws Throwable { 
		usertest2 = new Usuario ("manolo", "1234", "standard");
		assertTrue(usertest2.equals(usertest)); 
	}

	@Then("^The user will be granted access to the system$")
	public void The_user_will_be_granted_access_to_the_system() throws Throwable {
		assertTrue(usertest2.read());
		
		tearDown();
	}
	
	public void tearDown() {
		usertest.delete();
		usertest2.delete();
	}
}
