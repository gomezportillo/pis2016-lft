package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.domain.Usuario;

public class JUnitModifyUser {
	
	Usuario user, user2;
	
	@Given("^The admin has a created user$")
	public void The_admin_has_a_created_user() throws Throwable {
		user = new Usuario ("Manoli", "1234", "standard");
		user.create();
		assertTrue(user.read());
	}

	@When("^The admin modifies some data of this user$")
	public void The_admin_modifies_some_data_of_this_user() throws Throwable {
		user2 = new Usuario ("Pepa", "1234", "standard");
		assertFalse(user.equals(user2));
		user.update(user2);
	}

	@Then("^The user is correctly stored in the database$")
	public void The_user_is_correctly_stored_in_the_database() throws Throwable {
		assertFalse(user.read());
		assertTrue(user2.read());
		
		tearDown();
	}
	
	public void tearDown() {
		user.delete();
		user2.delete();
	}

}
