package main.persistance;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class MongoDBBroker {

	private String bbddname;
	private static MongoDBBroker _instance;
	private MongoClient client;
	private DB db;
	
	@SuppressWarnings("deprecation")
	private MongoDBBroker() {
		this.bbddname = "Mi_BBDD";	
		
		this.client = new MongoClient("localhost", 27017);
		this.db = client.getDB(bbddname);
	}

	public static MongoDBBroker getInstance() {
		if (_instance == null){
			_instance = new MongoDBBroker();
		}
		return _instance;
	}
	
	public DBCollection getCollection(String tabla){
		return this.db.getCollection(tabla);
	}
}
