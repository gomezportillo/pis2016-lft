package main.domain;

public class Usuario {
	private String nombre;
	private String pwd;
	private String rol;
	private DAOUsuario dao;

	public Usuario(String nombre, String pwd, String rol) {
		super();
		this.nombre = nombre;
		this.pwd = pwd;
		this.rol = rol;
		dao = new DAOUsuario(); 
	}

	public Usuario(){
		dao = new DAOUsuario();
	}
	public boolean read(){
		return dao.read(this);
	}

	public void readAll(){
		dao.readAll(this);
	}

	public void update(Usuario userNuevo){
		dao.update(this, userNuevo);
	}

	public void create(){
		dao.create(this);
	}

	public void delete(){
		dao.delete(this);
	}

	public String toString(){
		//return "Nombre: " + nombre + " " + pwd + " " + rol;
		return "Nombre: " + nombre + ", contraseņa: " + pwd + ", rol: " + rol;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String usuario) {
		this.nombre = usuario;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	public DAOUsuario getDAO(){
		return this.dao;
	}
	
	public boolean equals(Usuario u){
		return u.getNombre().equals(this.nombre) && u.getPwd().equals(this.pwd);
	}
}