package main.domain;

public class Tarea {
	private String nombre;
	private String usuario;
	private String prioridad;
	private String fecha_fin;
	private String proyecto;
	private String notas;
	private String estado; //pendiente editada borrada 
	private DAOTarea dao;
	
	public Tarea(String nombre, String usuario, String prioridad, String fecha_fin, String proyecto, String notas, String estado) {
		super();
		this.nombre = nombre;
		this.usuario = usuario;
		this.prioridad = prioridad;
		this.fecha_fin = fecha_fin;
		this.proyecto = proyecto;
		this.notas = notas;
		this.estado = estado;
		dao = new DAOTarea(); 
	}
	
	public Tarea(){
		dao = new DAOTarea(); 		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
		public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
		
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	
	public String getProyecto() {
		return proyecto;
	}
	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	public boolean read(){
	    return dao.read(this);
	  }
	
	public void readAll(){
		dao.readAll(this);
	}
	
	public boolean readByUser(String user){
		return dao.readByUser(user);
	}
	
	public void create(){
		dao.create(this);
	}
	
	public void delete(){
		dao.delete(this);
	}
	public void update( Tarea taskNueva){
		dao.update(this, taskNueva);
	}
	
	public String toString(){
		return "Nombre: " + nombre + ", usuario: " + usuario + ", prioridad: " + prioridad+ ", fecha: " +  fecha_fin + ", proyecto: " + proyecto + ", notas: " + notas+ ", estado: " + estado;
		
	}
	
	public boolean equals(Tarea t){
	    return (t.getNombre().equals(this.nombre) && t.getUsuario().equals(this.usuario) && 
	    		t.getPrioridad().equals(this.getPrioridad()) &&
	    		t.getFecha_fin().equals(this.fecha_fin) && t.getProyecto().equals(this.proyecto) && 
	    		t.getNotas().equals(this.notas) && t.getEstado().equals(this.estado));
	}
	
	public DAOTarea getDAO(){
		return this.dao;
	}

}