package main.domain;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import main.persistance.MongoDBBroker;

public class DAOUsuario {

	private ArrayList<Usuario> _collection;
	private String _tabla;

	//https://www.tutorialspoint.com/mongodb/mongodb_java.htm	

	public DAOUsuario() {
		this._collection = new ArrayList<Usuario>();
		this._tabla = "Usuarios";
	}

	public boolean read(Usuario usuario) {
		this._collection = new ArrayList<Usuario>();

		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);
		BasicDBObject doc = new BasicDBObject("nombre", usuario.getNombre());
		
		DBCursor cursor = coll.find(doc);
		DBObject tmpobj;
		String nombre, pwd, rol;
		Usuario u;
		
		if (!cursor.hasNext()) return false;
				
		while(cursor.hasNext()) {
			tmpobj = cursor.next();
			nombre = tmpobj.get("nombre").toString(); 
			pwd = tmpobj.get("pwd").toString();
			rol = tmpobj.get("rol").toString();
			u = new Usuario(nombre, pwd, rol);
			this._collection.add(u);
		}
		return true;
	}

	
	public void readAll(Usuario usuario) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);
		DBCursor cursor = coll.find();

		Usuario u;
		String nombre, pwd, rol;
		DBObject tmpobj;
		while(cursor.hasNext()) {
			tmpobj = cursor.next();
			nombre = tmpobj.get("nombre").toString(); 
			pwd = tmpobj.get("pwd").toString();
			rol = tmpobj.get("rol").toString();
			u = new Usuario(nombre, pwd, rol);
			this._collection.add(u);
		}
	}


	public void create(Usuario user) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);

		BasicDBObject doc = new BasicDBObject("nombre", user.getNombre());
		doc.append("rol", user.getRol());
		doc.append("pwd", user.getPwd());

		coll.insert(doc);
	}

	public void delete(Usuario user) {
		DBCollection coll = MongoDBBroker.getInstance().getCollection(_tabla);

		BasicDBObject doc = new BasicDBObject("nombre", user.getNombre());
		doc.append("rol", user.getRol());
		doc.append("pwd", user.getPwd());

		coll.remove(doc);
	} 

	public void update(Usuario user, Usuario userNuevo){
		this.delete(user);
		this.create(userNuevo);
	}
	
	public ArrayList<Usuario> getCollection(){
		return this._collection;
	}

}